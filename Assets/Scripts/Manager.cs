﻿using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public Text EnterField;
    private double first_value;
    private double second_value;
    private double resoult;
    private char Case;
    private bool firstTurn;
    private bool newNamber;
    private bool error;
    // Start is called before the first frame update
    void Start()
    {
        ClearCalc();
    }

    void ClearCalc()
    {
        EnterField.text = "0";
        first_value = second_value = resoult = 0;
        firstTurn = true;
        Case = '\0';
        error = false;
    }

    public void EnterNum(string num) 
    {
        if (EnterField.text.Length <= 9 || !error)
        {
            if (EnterField.text == "0" || newNamber)
            {
                EnterField.text = "";
                newNamber = false;
            }
            if (num == "," && EnterField.text == "")
            {
                EnterField.text = "0";
            }
            EnterField.text += num;
        }
    }

    public void BasicOperations(string operation)
    {
        first_value = double.Parse(EnterField.text);
        Сalculation();
        if (error == true) 
        {
            EnterField.text = "Деление на ноль невозможно";
            return;
        }
        switch (operation)
        {
            case "+":
                Case = '+';
                break;
            case "-":
                Case = '-';
                break;
            case "*":
                Case = '*';
                break;
            case "/":
                Case = '/';
                break;
            case "=":
                Case = '\0';
                firstTurn = true;
                break;
            default:
                break;
        }
        FieldText(); 
    }

    void FieldText() 
    {
        EnterField.text = resoult.ToString();
        newNamber = true;
    }
    void Сalculation()
    {
        if (firstTurn) 
        {
            second_value = first_value;
            firstTurn = false;
        }
        switch (Case)
        {
            case '+':
                second_value += first_value;
                break;
            case '-':
                second_value -= first_value;
                break;
            case '*':
                second_value *= first_value;
                break;
            case '/':
                if (first_value == 0)
                    error = true;
                else
                    second_value /= first_value;
                break;
            default:
                break;
        }
        resoult = second_value;
    }

    public void Cleaning(string typeClining)
    {
        switch (typeClining)
        {
            case "C":
                ClearCalc();
                break;
            case "CE":
                EnterField.text = "0";
                break;
            case "DELETE":
                EnterField.text = EnterField.text.Remove(EnterField.text.Length - 1);
                if (EnterField.text == "-" && !error)
                    EnterField.text = "0";
                break;
            default:
                break;
        }
    }

    public void NotBaseOperation(string operation)
    {
        if (!error)
        {
            switch (operation)
            {
                case "+/-":
                    EnterField.text = (double.Parse(EnterField.text) * -1).ToString();
                    break;
                case "%":
                    EnterField.text = (double.Parse(EnterField.text) / 100).ToString();
                    break;
                case "root":
                    if (double.Parse(EnterField.text) > 0)
                        EnterField.text = Mathf.Sqrt(float.Parse(EnterField.text)).ToString();
                    else
                    {
                        EnterField.text = "Из отрицательного числа нельзя вычислять корень";
                        error = true;
                    }
                    break;
                case "^":
                    EnterField.text = Mathf.Pow((float.Parse(EnterField.text)), 2).ToString();
                    break;
                case "1/x":
                    if (float.Parse(EnterField.text) != 0)
                        EnterField.text = (1 / (double.Parse(EnterField.text))).ToString();
                    else
                    {
                        EnterField.text = "Деление на ноль невозможно";
                        error = true;
                    }
                    break;
                default:
                    break;
            } 
        }
    }
}
